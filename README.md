## Ridiculous Clone

# Group Members

- Antonio Merendaz - C0741427 
- Carlos Bulado - C0734506
- Giselle Tavares - C0744277


## Problem Description

Okay - so the original assignment was pretty ridiculous. So we’re changing it to be even MORE RIDICULOUS!
  
Welcome to RIDICULOUS FISHING!  [Gameplay](https://www.youtube.com/watch?v=5mOVjiPbMYI)

In this assignment, you will create a clone of RIDICULOUS FISHING, the Apple App Store’s 2013 Game of the Year.

## To Do List
* ✅ Different fish & sea creatures
	* ✅ Good fish
	* ✅ Rare fish → example, the fish that only comes out at midnight
	* ✅ Bad fish
* ✅ Different guns
	* ✅ Create a start scene that will allow the player pick one gun to be used on the game using the money that the player starts
* ✅ Ability to catch fish
* ✅ The enemy (octopus) must appear randomly
* ✅ Ability to bring fish back to surface
* ✅ Launch the fishes on the sky right after the surface - the fishes must have physics. some can go up more than another (different mass?)
* ✅ When fish come back to surface, SHOOT THEM!
* ✅ Points / coins
	* ✅ First time playing, the player starts with $500
	* ✅ The player wins coins for each fish shooted
* ✅ Rewards system
* HIGH SCORES system
	* ✅ Score = max meters reached ```*``` coins earned during the game 
	* Save on the plist top 5
	* Save on the plist also the money earned
* ✅ Secrets ! 
	* ✅ There is a treasure in the very deep ocean. If catch it, get more points
* ✅ Appropriate sound/art
	* ✅ The background to the bottom and to the top must be in a loop.
* ✅ Appropriate controls 
	* ✅ The line moves only to right and left to the bottom. The Y is fixed. Once the line intersect a fish (good), the line go back to the top.
	* ✅ The line can go down, if it doesn't touch in any fish, until reach the max meters
* ✅ Meters count
	* ✅ Must have a fix meter, that will be the max deep on the ocean
	* ✅ Must have a counter for the meter when in line is going to the bottom. Once reached the fix meter (max meter), it should go back to the surface, and this counter should be decremented
* Win / Lose screen + play again
	* play again must considering the coins earned so far (take it from the plist)
* ✅ Humor!
* ✅ Similar mechanics to original Ridiculous Fishing game



-------------
**✅ Done**

**🚩 Doing**

