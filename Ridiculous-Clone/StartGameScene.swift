//
//  StartGameScene.swift
//  Ridiculous-Clone
//
//  Created by Giselle Tavares on 2019-10-23.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class StartGameScene: SKScene
{
    var buttonGunZero = SKSpriteNode()
    var buttonGunOne = SKSpriteNode()
    var buttonGunTwo = SKSpriteNode()
    var cantBuyLabel = SKLabelNode()
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var audioPlayer = AVAudioPlayer()
    
    override func didMove(to view: SKView)
    {
        // Play the Background Music
        BackMusic.instance.playBackMusic()
        // Make a background sky
        let background_sky = SKSpriteNode(imageNamed:"sky-background")
        background_sky.size.width = 2300
        background_sky.size.height = 10000
        background_sky.position = CGPoint(x:self.size.width/2, y:6000)
        addChild(background_sky)
        
        // Make a background sea
        let background_sea = SKSpriteNode(imageNamed:"sea-background")
        background_sea.size.width = 2300
        background_sea.size.height = 10000
        background_sea.position = CGPoint(x:self.size.width/2, y:-4000)
        addChild(background_sea)
        
        // Make a wave
        let wave = SKSpriteNode(imageNamed:"wave")
        wave.size.width = 2300
        wave.size.height = 100
        wave.position = CGPoint(x:self.size.width/2, y:950)
        wave.zPosition = 11
        addChild(wave)
        
        // Make a wave
        let wave2 = SKSpriteNode(imageNamed:"wave")
        wave2.size.width = 2300
        wave2.size.height = 100
        wave2.position = CGPoint(x:self.size.width/2 - 30, y:980)
        wave2.zPosition = 11
        addChild(wave2)
        
        // Game Title
        let gameTitle = SKLabelNode(text: "Ridiculous Fishing Clone")
        gameTitle.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        gameTitle.fontColor = UIColor.orange
        gameTitle.fontSize = 120
        gameTitle.fontName = "Avenir"
        addChild(gameTitle)
        
        // Game Title
        let instructions = SKLabelNode(text: "Pick a gun to start the game")
        instructions.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2 - 80)
        instructions.fontColor = UIColor.brown
        instructions.fontSize = 60
        instructions.fontName = "Avenir"
        addChild(instructions)
        
        // Coins
        let coins = SKLabelNode(text: "Coins: $\(GameScene.coins)")
        coins.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2 - 160)
        coins.fontColor = UIColor.brown
        coins.fontSize = 60
        coins.fontName = "Avenir"
        addChild(coins)
        
        // guns buttons
        self.buttonGunZero = SKSpriteNode(imageNamed: "gun0")
        self.buttonGunZero.size.width = 450
        self.buttonGunZero.size.height = 250
        self.buttonGunZero.position = CGPoint(x: frame.midX - 400, y: frame.midY*0.6)
        addChild(self.buttonGunZero)
        
        self.buttonGunOne = SKSpriteNode(imageNamed: "gun1")
        self.buttonGunOne.size.width = 450
        self.buttonGunOne.size.height = 250
        self.buttonGunOne.position = CGPoint(x: frame.midX, y: frame.midY*0.6)
        addChild(self.buttonGunOne)
        
        self.buttonGunTwo = SKSpriteNode(imageNamed: "gun2")
        self.buttonGunTwo.size.width = 450
        self.buttonGunTwo.size.height = 250
        self.buttonGunTwo.position = CGPoint(x: frame.midX + 400, y: frame.midY*0.6)
        addChild(self.buttonGunTwo)
        
        // Guns $ labels
        let gunZeroValue = SKLabelNode(text: "FREE")
        gunZeroValue.position = CGPoint(x: frame.midX - 400, y: frame.midY*0.4)
        gunZeroValue.fontColor = UIColor.orange
        gunZeroValue.fontSize = 60
        gunZeroValue.fontName = "Avenir"
        addChild(gunZeroValue)
        
        let gunOneValue = SKLabelNode(text: "$300")
        gunOneValue.position = CGPoint(x: frame.midX, y: frame.midY*0.4)
        gunOneValue.fontColor = UIColor.orange
        gunOneValue.fontSize = 60
        gunOneValue.fontName = "Avenir"
        addChild(gunOneValue)
        
        let gunTwoValue = SKLabelNode(text: "$200")
        gunTwoValue.position = CGPoint(x: frame.midX + 400, y: frame.midY*0.4)
        gunTwoValue.fontColor = UIColor.orange
        gunTwoValue.fontSize = 60
        gunTwoValue.fontName = "Avenir"
        addChild(gunTwoValue)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touch: UITouch = touches.first! as UITouch
        let location: CGPoint = touch.location(in: self)
        
        if buttonGunZero.contains(location) {
            sceneManagerDelegate?.presentGameSceneFor(gun: 0)
        }
       
        if buttonGunOne.contains(location) && GameScene.coins >= 300 {
            sceneManagerDelegate?.presentGameSceneFor(gun: 1)
        } else if buttonGunOne.contains(location) && GameScene.coins < 300 {
            cantBuy()
        }
        
        if buttonGunTwo.contains(location) && GameScene.coins >= 200 {
            sceneManagerDelegate?.presentGameSceneFor(gun: 2)
        } else if buttonGunTwo.contains(location) && GameScene.coins < 200 {
            cantBuy()
        }
    }
    
    func cantBuy(){
        self.cantBuyLabel = SKLabelNode(text: "Too expensive for you!")
        self.cantBuyLabel.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2 - 220)
        self.cantBuyLabel.fontColor = UIColor.red
        self.cantBuyLabel.fontSize = 50
        self.cantBuyLabel.fontName = "Avenir"
        addChild(self.cantBuyLabel)
    }
}
