//
//  HighScoreManager.swift
//  Ridiculous-Clone
//
//  Created by Giselle Tavares on 2019-10-24.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import Foundation

class HighScoreManager {
    var scores:Array<HighScore> = [];
    
    init() {
        // load existing high scores or set up an empty array
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0] as String
        let path = NSTemporaryDirectory().stringByAppendingPathComponent(path: "HighScores.plist")
//        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("HighScores.plist")
//        let path = documentsDirectory.stringByAppendingPathComponent("HighScores.plist")
        let fileManager = FileManager.default
        
        // check if file exists
        if !fileManager.fileExists(atPath: path) {
            // create an empty file if it doesn't exist
            if let bundle = Bundle.main.path(forResource: "DefaultFile", ofType: "plist") {
//                fileManager.copyItemAtPath(bundle, toPath: path, error:nil)
//                fileManager.copyItem(atPath: bundle, toPath: path)
                do {
                    try fileManager.copyItem(atPath: bundle, toPath: path)
                } catch {
                    
                }
            }
        }
        
        if let rawData = NSData(contentsOfFile: path) {
            // do we get serialized data back from the attempted path?
            // if so, unarchive it into an AnyObject, and then convert to an array of HighScores, if possible
            var scoreArray: AnyObject? = NSKeyedUnarchiver.unarchiveObject(with: rawData as Data) as AnyObject?;
            self.scores = scoreArray as? [HighScore] ?? [];
        }
    }
    
    func save() {
        // find the save directory our app has permission to use, and save the serialized version of self.scores - the HighScores array.
        let saveData = NSKeyedArchiver.archivedData(withRootObject: self.scores);
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray;
        let documentsDirectory = paths.object(at: 0) as! NSString;
        let path = documentsDirectory.appendingPathComponent("HighScores.plist");
        
        do {
           try saveData.write(to: URL(fileURLWithPath: path), options: .atomic)
        } catch let error {
            print(error)
        }
        
//        saveData.write(to: URL(fileURLWithPath: path), options: .atomic)
//        saveData.writeToFile(path, atomically: true);
    }
    
    // a simple function to add a new high score, to be called from your game logic
    // note that this doesn't sort or filter the scores in any way
    func addNewScore(newScore:Int) {
        let newHighScore = HighScore(score: newScore, dateOfScore: NSDate());
        self.scores.append(newHighScore);
        self.save();
    }
}

extension String {
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
}
