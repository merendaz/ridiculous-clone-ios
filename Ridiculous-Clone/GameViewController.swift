//
//  GameViewController.swift
//  Ridiculous-Clone
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-10-15.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

protocol SceneManagerDelegate
{
    func presentStartGameScene()
    func presentGameSceneFor(gun: Int)
}

class GameViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        presentStartGameScene()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask
    {
        if UIDevice.current.userInterfaceIdiom == .phone { return .portrait }
        else { return .all }
    }

    override var prefersStatusBarHidden: Bool { return true }
}

extension GameViewController: SceneManagerDelegate
{
    func presentStartGameScene()
    {
        let startGameScene = StartGameScene(size:CGSize(width: 2048, height: 1536))
        startGameScene.sceneManagerDelegate = self
        present(scene: startGameScene)
    }
    
    func presentGameSceneFor(gun: Int)
    {
        let gameScene = GameScene(size:CGSize(width: 2048, height: 1536))
        gameScene.sceneManagerDelegate = self
        gameScene.gun = gun
        present(scene: gameScene)
    }
    
    func present(scene: SKScene)
    {
//        print("came to the present scene")
        let skView = self.view as! SKView
//        scene.scaleMode = .fill // default
        skView.presentScene(scene)
    }
}
