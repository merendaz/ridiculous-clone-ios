//
//  BackMusic.swift
//  Ridiculous-Clone
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-10-24.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import AVFoundation

class BackMusic: NSObject {
    static let instance = BackMusic()
    var audioPlayer = AVAudioPlayer()
    
   func playBackMusic() {
        // Loading the Background Music:
        if let musicURL = Bundle.main.url(forResource: "Sounds_Music/gone_fishin_BACKGRND", withExtension: "mp3") {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: musicURL)
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            }
            catch {
                print("Couldn't play the music!")
            }
        } else {
        print("ERROR! URL not found!")
        return
        }
    }
    
    func pauseMusic(){
        audioPlayer.pause()
    }
    func playMusic(){
        audioPlayer.play()
    }
}

