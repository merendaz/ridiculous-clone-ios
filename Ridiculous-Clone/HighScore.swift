//
//  HighScore.swift
//  Ridiculous-Clone
//
//  Created by Giselle Tavares on 2019-10-24.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import Foundation

// inherit from NSCoding to make instances serializable
class HighScore: NSObject, NSCoding {
    let score:Int;
    let dateOfScore:NSDate;
    
    init(score:Int, dateOfScore:NSDate) {
        self.score = score;
        self.dateOfScore = dateOfScore;
    }
    
    required init(coder: NSCoder) {
        self.score = coder.decodeObject(forKey: "score")! as! Int;
        self.dateOfScore = coder.decodeObject(forKey: "dateOfScore")! as! NSDate;
        super.init()
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.score, forKey: "score")
        coder.encode(self.dateOfScore, forKey: "dateOfScore")
    }
}
