//
//  GameScene.swift
//  Ridiculous-Clone
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-10-15.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene
{
    // ------------------------
    static var coins = 500
    // ------------------------
    var shoreBackground : SKSpriteNode! = nil
    var oceanBackground1 : SKSpriteNode! = nil
    var oceanBackground2 : SKSpriteNode! = nil
    var oceanBackground3 : SKSpriteNode! = nil
    var oceanBottomBackground : SKSpriteNode! = nil
    var movingUp : Bool = false
    var isGameStarted : Bool = false
    
    var ship : SKSpriteNode! = nil
    
    /*
     Explanation for playerMovement property:
      0 - Initial Value - No moviment
      1 - Moving Left
      2 - Moving Right
     */
    var playerMovement : Int = 0
    var bottomPositionForPlayer : CGPoint = CGPoint(x: 10, y: 10000)
    
    var gameMovement : CGPoint = CGPoint(x: 20, y: 20)
    var quantityOfScreensDown : Int = 0
    var maxQuantityOfScreenDown : Int = 30
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var gun: Int!
    var gunName: String! = ""
    var bonus: Int = 0
    
    // MARK: Sprites
    // -------------------------
    var playerhook:SKSpriteNode!
//    var eviloctopus:SKSpriteNode!
//    var treasure:SKSpriteNode!
    
    var livesLabel:SKLabelNode!
    var metersLabel:SKLabelNode!
    var coinLabel:SKLabelNode!
    var scoreLabel:SKLabelNode!
    
    var fishes:[SKSpriteNode] = []
    var enemies:[SKSpriteNode] = []
    var treasures:[SKSpriteNode] = []
    var caughtFishesArray: [SKSpriteNode] = []
    var fishesGoingDown : [SKSpriteNode] = []
    
    var commonFishes = ["fish_blue", "fish_black", "fish_gold_orange", "fish_color2", "fish_orange_green_strip", "fish_mini_red", "fish_black_pink", "fish_color1", "fish_happy"]
    var rareFishes = ["fish_blue_RARE", "fish_strip_yell_black_RARE", "fish_color3_RARE", "fish_lantern_BAD"]
    var enemy = ["EvilOctopus_BAD"]
    var treasure = ["treasurechestSmall"]
    
    var lives = 100
    var meters = 0
    var maxMeters = 65
    var metersReached = 0
    var score = 0
    
    var mouseX:CGFloat = 0
    var mouseY:CGFloat = 0
    
    var isOnSurface: Bool = false
    var isGameOver: Bool = false
    
    let spriteSpeed: CGFloat = 3
    
    var count = 0
    var fishFacing = "right"
    
    // Sound Effects
    let bloopSound =
        SKAction.playSoundFileNamed("Sounds_Music/bloop.mp3",
                                    waitForCompletion: false)
    let bulletOnTgtSound =
        SKAction.playSoundFileNamed("Sounds_Music/bulletOnTgt.mp3",
                                    waitForCompletion: false)
    let coinSound =
        SKAction.playSoundFileNamed("Sounds_Music/coin.mp3",
                                    waitForCompletion: false)
    let fish_rareSound =
        SKAction.playSoundFileNamed("Sounds_Music/fish_rare.mp3",
                                    waitForCompletion: false)
    let gunshotSound =
        SKAction.playSoundFileNamed("Sounds_Music/gunshot.mp3",
                                    waitForCompletion: false)
    let octopus_biteSound =
        SKAction.playSoundFileNamed("Sounds_Music/octopus_bite.wav",
                                    waitForCompletion: false)
    let point_specialSound =
        SKAction.playSoundFileNamed("Sounds_Music/point_special.mp3",
                                    waitForCompletion: false)
    let reelSound =
        SKAction.playSoundFileNamed("Sounds_Music/reel.mp3",
                                    waitForCompletion: false)

    override func didMove(to view: SKView)
    {
        self.setBackground()

        // Set the background color of the app
        self.backgroundColor = SKColor.black;
        
        // reduce the coins to purchase the gun picked
        purchaseTheGun()
        
        // Add a ship
        self.ship = SKSpriteNode(imageNamed:"viking_ship")
        self.ship.size.width = 1450
        self.ship.size.height = 700
        self.ship.zPosition = 101
        self.ship.position = CGPoint(x: self.size.width / 2, y: 1300)
        addChild(self.ship)
        
        // Add your player
        self.playerhook = SKSpriteNode(imageNamed: "fishhook")
        self.playerhook.zPosition = 102
        self.playerhook.position = CGPoint(x: self.size.width / 2, y: 1000)
        addChild(playerhook)
        self.playerhook.physicsBody?.affectedByGravity = true
        
        // Add a coins label
        self.coinLabel = SKLabelNode(text: "Coins: $\(GameScene.coins)")
        self.coinLabel.position = CGPoint(x:100, y:1450)
        self.coinLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        self.coinLabel.fontColor = UIColor.yellow
        self.coinLabel.fontSize = 65
        self.coinLabel.fontName = "AvenirNext-Bold"
        self.coinLabel.zPosition = 102
        addChild(self.coinLabel)
        
        // Add a meters label
        self.metersLabel = SKLabelNode(text: "\(self.meters) M / Max: \(self.maxMeters) M")
        self.metersLabel.position = CGPoint(x:100, y:1380)
        self.metersLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        self.metersLabel.fontColor = UIColor.magenta
        self.metersLabel.fontSize = 65
        self.metersLabel.fontName = "Avenir"
        self.metersLabel.zPosition = 102
        addChild(self.metersLabel)
        
        // Add life label
        self.livesLabel = SKLabelNode(text: "Lives Remaining: \(lives)")
        self.livesLabel.position = CGPoint(x:100, y:1310)
        self.livesLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        self.livesLabel.fontColor = UIColor.magenta
        self.livesLabel.fontSize = 65
        self.livesLabel.fontName = "Avenir"
        self.livesLabel.zPosition = 102
        addChild(self.livesLabel)
        
    }
    
    func purchaseTheGun() {
        switch gun {
            case 0:
                GameScene.coins -= 0
                self.bonus = 0
                self.gunName = "gun0"
            case 1:
                GameScene.coins -= 300
                self.bonus = 15
                self.gunName = "gun1"
            case 2:
                GameScene.coins -= 200
                self.bonus = 5
                self.gunName = "gun2"
            default:
                break
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let locationTouched = touches.first else { return }
        let mousePosition = locationTouched.location(in:self)
        
        if mousePosition.x < self.playerhook.position.x { self.playerMovement = 1 }
        else { self.playerMovement = 2 }
        
        self.mouseX = mousePosition.x
        self.mouseY = mousePosition.y
        
        if self.isGameOver {
            sceneManagerDelegate?.presentStartGameScene()
        }
        
        // gain coins when shot the fishes
        shootTheFishes(mousePosition: mousePosition)
        
        //SOUND EFFECT
        if (!isGameStarted && !self.movingUp) {
            self.run(bloopSound)
            self.run(reelSound)
        }
        else if ((self.movingUp && self.isOnSurface) && ((self.caughtFishesArray.count != 0 || self.fishesGoingDown.count != 0))){
            self.run(gunshotSound)
        }
        self.isGameStarted = true
    }
    
    // =============================================
    // ========== MOVING TO TARGET =================
    // =============================================
    func moveSpriteToTarget(spriteToMoveTo:SKSpriteNode, targetXPosition:CGFloat, targetYPosition:CGFloat)
    {
        // Move the sprite towards the target
        // 1. Calculate distance between sprite and target
        let x = (targetXPosition - spriteToMoveTo.position.x);
        let y = (targetYPosition - spriteToMoveTo.position.y);
        let distance = sqrt((x * x) + (y * y))
        
        // 2. Calculate the "rate" to move
        let xn = (x / distance)
        let yn = (y / distance)
        
        // 3. Move the sprite
        spriteToMoveTo.position.x = spriteToMoveTo.position.x + (xn * 10);
        spriteToMoveTo.position.y = spriteToMoveTo.position.y + (yn * 10);
    }
    
    
    // SAME AS UPDATEPOSITIONS
    
    var numLoops = 0
    
    override func update(_ currentTime: TimeInterval)
    {
        if isGameStarted
        {
            numLoops = numLoops + 1
            // Spawning common fishes
            if (numLoops % 20 == 0){
                self.spawnFish(fishesArr: self.commonFishes)
            }
            
            // Spawning rare fishes
            if (numLoops % 101 == 0){
                self.spawnFish(fishesArr: self.rareFishes)
            }
            
            // Spawning the enemies
            if (numLoops % 50 == 0) {
                self.spawnEnemy(enemyArr: self.enemy)
            }
            
            // couting meters down and up
            if(numLoops % 20 == 0){
                if(!self.movingUp){
                    self.meters += 1
                    self.metersReached = self.meters
                    
                    if(self.meters == 60){
                        self.spawnTreasure(treasureArr: self.treasure)
                    }
                } else if(self.movingUp && !self.isOnSurface) {
                    self.meters -= 1
                } else if(self.isOnSurface) {
                    self.meters = 0
                }
                self.metersLabel.text = "\(self.meters) M / Reached: \(self.metersReached) M / Max: \(self.maxMeters) M"
            }

            // Keep a maximum number of enemies in the scene (it depends on the number of loops)
            // Always removing one
            if ((self.enemies.count > 30) && (numLoops % 200 == 0)) {
                self.enemies.first?.removeFromParent()
                self.enemies.remove(at:0)
            }
            
            // Detect when playerhook and eviloctopus collide
            for (index, enemy) in self.enemies.enumerated() {
                if self.playerhook.frame.intersects(enemy.frame) {
                    //print("\(currentTime): COLLISON!")
                    self.lives = self.lives - 1
                    
//                    //SOUND EFFECT
//                    self.run(octopus_biteSound)

                    // update the life counter
                    self.livesLabel.text = "Lives Remaining: \(lives)"

                    // MARK TODO => NEW LOSE SCREEN
        //            // SHOW LOSE SCREEN
        //            let loseScene = LoseScreen(size: self.size)
        //            //let transitionEffect = SKTransition.flipHorizontal(withDuration: 2)
        //            self.view?.presentScene(loseScene)
        //            //self.view?.presentScene(loseScene, transition:transitionEffect)

                }
            }
            
            
            // Detect collisions between playerhook and fishes
            
            // Loop through your array of fishs
            // If the player is touching the fish, then remove the fish & increase the score
            if self.playerhook.position.y > 0
            {
                var caughtFishesArrayNow : [SKSpriteNode] = []
                for (index, fish) in self.fishes.enumerated()
                {
                    if self.playerhook.frame.intersects(fish.frame)
                    {
                        //Pausing the Music
                        BackMusic.instance.pauseMusic()
                        //SOUND EFFECT
                        self.run(fish_rareSound)
                        
                        fish.position.x = self.playerhook.position.x
                        fish.position.y = self.playerhook.position.y
                        
                        fish.run(SKAction.rotate(byAngle: CGFloat(Double.pi/2), duration: 5))
                        
                        self.movingUp = true
                    
                        caughtFishesArrayNow.append(fish)
                    }
                }
                
                for (index, treasure) in self.treasures.enumerated() {
                    if self.playerhook.frame.intersects(treasure.frame) {
                        //Pausing the Music
                        BackMusic.instance.pauseMusic()
                        
                        //SOUND EFFECT
                        self.run(point_specialSound)
                        
                        treasure.position.x = self.playerhook.position.x
                        treasure.position.y = self.playerhook.position.y
                        
                        treasure.run(SKAction.rotate(byAngle: CGFloat(Double.pi/2), duration: 5))
                        
                        self.movingUp = true
                        
                        caughtFishesArrayNow.append(treasure)
                    }
                }
                
                self.caughtFishesArray = caughtFishesArrayNow
            }
            
            if(isOnSurface && self.caughtFishesArray.count == 0 && self.fishesGoingDown.count == 0){
                self.gameOver()
            }

            if movingUp { self.ship.position.y = self.ship.position.y - self.gameMovement.y } else { self.ship.position.y = self.ship.position.y + self.gameMovement.y }
            self.moveBackground()
            self.moveFish()
            self.moveEnemy()
            self.moveTreasure()
            self.movePlayer()
            self.fishingUp()
        }
    }
}


// Extension exclusively to treat background
extension GameScene
{
    func setBackground()
    {
        // Set the background color of the app
        self.backgroundColor = SKColor.black

        // Shore background
        self.shoreBackground = SKSpriteNode(imageNamed: "backgrnd_cartoon_small_02")
        self.shoreBackground.size = self.size
        self.shoreBackground.zPosition = 100
        self.shoreBackground.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        addChild(self.shoreBackground)

        // First ocean background
        self.oceanBackground1 = SKSpriteNode(imageNamed: "backgrnd_cartoon_small_03")
        self.oceanBackground1.size.width = self.size.width
        self.oceanBackground1.size.height = self.size.height + 300
        var y : CGFloat = (-1 * self.oceanBackground1.frame.height / 2) + 40
        self.oceanBackground1.position = CGPoint(x: self.size.width / 2, y: y)
        addChild(self.oceanBackground1)
        
        // Second ocean background
        self.oceanBackground2 = SKSpriteNode(imageNamed: "backgrnd_cartoon_small_03")
        self.oceanBackground2.size.width = self.size.width
        self.oceanBackground2.size.height = self.size.height + 300
        y = y - (-1 * self.oceanBackground2.frame.height / 2) + 40
        self.oceanBackground2.position = CGPoint(x: self.size.width / 2, y: y)
        addChild(self.oceanBackground2)
        
        // Third ocean background
        self.oceanBackground3 = SKSpriteNode(imageNamed: "backgrnd_cartoon_small_03")
        self.oceanBackground3.size.width = self.size.width
        self.oceanBackground3.size.height = self.size.height + 300
        y = y - (-1 * self.oceanBackground3.frame.height / 2) + 40
        self.oceanBackground3.position = CGPoint(x: self.size.width / 2, y: y)
        addChild(self.oceanBackground3)

        // Bottom of ocean background
//        self.oceanBottomBackground = SKSpriteNode(imageNamed: "backgrnd_cartoon_small_GROUND")
//        self.oceanBottomBackground.size.width = self.size.width
//        self.oceanBottomBackground.position = CGPoint(x: self.size.width / 2, y: -1000)
//        addChild(self.oceanBottomBackground)
    }

    func moveBackground()
    {
        if (self.shoreBackground.position.y + self.shoreBackground.frame.height / 2) > 0 || (self.movingUp && self.playerhook.position.y > self.bottomPositionForPlayer.y)
        {
            if !movingUp
            {
                self.shoreBackground.position.y = self.shoreBackground.position.y + self.gameMovement.y
                self.oceanBackground1.position.y = self.oceanBackground1.position.y + self.gameMovement.y
                self.oceanBackground2.position.y = self.oceanBackground2.position.y + self.gameMovement.y
                self.oceanBackground3.position.y = self.oceanBackground3.position.y + self.gameMovement.y
                
                if self.oceanBackground1.position.y > self.size.height
                {
                    self.oceanBackground1.position.y = self.oceanBackground3.position.y - (self.oceanBackground1.frame.height / 2)
                    self.quantityOfScreensDown = self.quantityOfScreensDown + 1
                }
                if self.oceanBackground2.position.y > self.size.height
                {
                    self.oceanBackground2.position.y = self.oceanBackground1.position.y - (self.oceanBackground2.frame.height / 2)
                    self.quantityOfScreensDown = self.quantityOfScreensDown + 1
                }
                if self.oceanBackground3.position.y > self.size.height
                {
                    self.oceanBackground3.position.y = self.oceanBackground2.position.y - (self.oceanBackground3.frame.height / 2)
                    self.quantityOfScreensDown = self.quantityOfScreensDown + 1
                }
            }
            else
            {
                self.shoreBackground.position.y = self.shoreBackground.position.y - self.gameMovement.y
                self.oceanBackground1.position.y = self.oceanBackground1.position.y - self.gameMovement.y
                self.oceanBackground2.position.y = self.oceanBackground2.position.y - self.gameMovement.y
                self.oceanBackground3.position.y = self.oceanBackground3.position.y - self.gameMovement.y
                
                if self.oceanBackground1.position.y < self.size.height
                {
                    self.oceanBackground3.position.y = self.oceanBackground1.position.y + (self.oceanBackground3.frame.height / 2)
                }
                if self.oceanBackground2.position.y < self.size.height
                {
                    self.oceanBackground1.position.y = self.oceanBackground2.position.y + (self.oceanBackground1.frame.height / 2)
                }
                if self.oceanBackground3.position.y < self.size.height
                {
                    self.oceanBackground2.position.y = self.oceanBackground3.position.y + (self.oceanBackground2.frame.height / 2)
                }
            }

            if self.quantityOfScreensDown == self.maxQuantityOfScreenDown { movingUp = true }
        }
    }
}


// Extension exclusively to treat fish
extension GameScene
{
    func spawnFish(fishesArr:[String])
    {
        if !movingUp
        {
            count += 1
            let randomIndex = Int.random(in: 0 ... fishesArr.count-1)
            let aFish = fishesArr[randomIndex]
            // Add a fish to a static location
            let fish = SKSpriteNode(imageNamed: aFish)
            
            // generate a random x position
            
            let randomXPos = CGFloat.random(in: 0 ... size.width)
            //let randomYPos = -20 //CGFloat.random(in: 70 ... 950)
            fish.position = CGPoint(x: randomXPos, y: -20)
            
            // change the fish's direction
            if fish.position.x >= self.size.width / 2
            {
                let faceLeft = SKAction.scaleX(to: -1, duration: 0)
                fish.run(faceLeft)
                // save fish's position
                self.fishFacing = "left"
                fish.userData = ["imageName" : aFish]
                fish.userData = ["fishfacing" : self.fishFacing]
            }
            else if fish.position.x < self.size.width / 2
            {
                // save fish's position
                self.fishFacing = "right"
            }

            // add the fish to the screen
            fish.zPosition = 102
            addChild(fish)
            // add the fish to the array
            self.fishes.append(fish)
        }
    }
    
    func moveFish()
    {
        if (self.shoreBackground.position.y + self.shoreBackground.frame.height / 2) > 0
        {
            if !movingUp
            {
                for fish in self.fishes
                {
                    fish.position.y = fish.position.y + self.gameMovement.y
                    if (self.fishFacing == "right") {
                        fish.position.x = fish.position.x + self.gameMovement.x/2
                    }
                    else {
                        fish.position.x = fish.position.x - self.gameMovement.x/2
                    }
                }
            }
            else
            {
                for fish in self.fishes
                {
                    fish.position.y = fish.position.y - self.gameMovement.y
                    if (self.fishFacing == "right") {
                        fish.position.x = fish.position.x + self.gameMovement.x/2
                    }
                    else {
                        fish.position.x = fish.position.x - self.gameMovement.x/2
                    }
                }
            }
        }
    }
    
    func fishingUp()
    {
        if self.playerhook.position.y < 0 && self.caughtFishesArray.count > 0
        {
            self.caughtFishesArray.first!.position.y = self.caughtFishesArray.first!.position.y + self.gameMovement.y
            if self.caughtFishesArray.first!.position.x == self.playerhook.position.x
            {
                let randomXPosition = Int.random(in: 100 ... Int(self.size.width) - 100)
                self.caughtFishesArray.first!.position.x = CGFloat(randomXPosition)
            }
            if self.caughtFishesArray.first!.position.y > self.size.height + 100
            {
                self.fishesGoingDown.append(self.caughtFishesArray.first!)
                self.caughtFishesArray.remove(at: 0)
            }
        }
        
        if self.playerhook.position.y < 0 && self.fishesGoingDown.count > 0
        {
            if self.fishesGoingDown.first!.position.y > self.size.height + 100
            {
                let randomXPosition = Int.random(in: 100 ... Int(self.size.width) - 100)
                self.fishesGoingDown.first!.position.x = CGFloat(randomXPosition)
            }
            self.fishesGoingDown.first!.position.y = self.fishesGoingDown.first!.position.y - self.gameMovement.y
            
            if self.fishesGoingDown.first!.position.y < -100
            {
                self.fishesGoingDown.remove(at: 0)
            }
        }
        
        if self.playerhook.position.y < 0 && self.caughtFishesArray.count == 0 && self.fishesGoingDown.count == 0
        {
            self.isGameStarted = false
            // @TODO: Calculations with fish and coins
            // It's done. On TouchEnd there is already the calculation for the coins for each fish hit
            
            // @TODO: Change page to choose a weapon page
        }
    }
}

// Extension exclusively to treat enemy
extension GameScene {
    func spawnEnemy(enemyArr:[String]) {
        if !movingUp {
            count += 1
            let randomIndex = Int.random(in: 0 ... enemyArr.count-1)
            let aEnemy = enemyArr[randomIndex]
            let enemy = SKSpriteNode(imageNamed: aEnemy)
            
            // generate a random x position
            let randomXPos = CGFloat.random(in: 0 ... size.width)
            enemy.position = CGPoint(x: randomXPos, y: -20)
            
            // change the fish's direction
            if enemy.position.x >= self.size.width / 2 {
                let faceLeft = SKAction.scaleX(to: -1, duration: 0)
                enemy.run(faceLeft)
                
                // the same variable for the fishes can be used here
                self.fishFacing = "left"
                enemy.userData = ["imageName" : aEnemy]
                enemy.userData = ["fishfacing" : self.fishFacing]
            } else if enemy.position.x < self.size.width / 2 {
                self.fishFacing = "right"
            }

            // add the fish to the screen
            enemy.zPosition = 102
            addChild(enemy)
            // add the fish to the array
            self.enemies.append(enemy)
        }
    }
    
    func moveEnemy() {
        if (self.shoreBackground.position.y + self.shoreBackground.frame.height / 2) > 0 {
            if !movingUp {
                for enemy in self.enemies {
                    enemy.position.y = enemy.position.y + self.gameMovement.y
                    enemy.position.x = enemy.position.x + self.gameMovement.y
                }
            } else {
                for enemy in self.enemies {
                    enemy.position.y = enemy.position.y - self.gameMovement.y
                }
            }
        }
    }
}

// Extension exclusively to treat player
extension GameScene
{
    func movePlayer()
    {
        if self.playerhook.position.y < self.ship.position.y
        {
            if self.playerMovement == 1 && self.playerhook.position.x - self.gameMovement.x > 0  { self.playerhook.position.x = self.playerhook.position.x - self.gameMovement.x }
            if self.playerMovement == 2 && self.playerhook.position.x + self.gameMovement.x < self.size.width { self.playerhook.position.x = self.playerhook.position.x + self.gameMovement.x }
            if self.movingUp && self.playerhook.position.y > self.bottomPositionForPlayer.y { self.playerhook.position.y = self.playerhook.position.y - self.gameMovement.y }
        }
        else
        {
            self.playerhook.position.y = self.ship.position.y
            self.isOnSurface = true
        }
    }
}

extension GameScene {
    func spawnTreasure(treasureArr: [String]){
        // Add a treasure chest (The secret)
        if !movingUp {
            count += 1
            let randomIndex = Int.random(in: 0 ... treasureArr.count-1)
            let aTreasure = treasureArr[randomIndex]
            let treasure = SKSpriteNode(imageNamed: aTreasure)
            
            // generate a random x position
            let randomXPos = CGFloat.random(in: 0 ... size.width)
            treasure.position = CGPoint(x: randomXPos, y: -20)

            // add the treasure to the screen
            treasure.zPosition = 102
            addChild(treasure)
            // add the fish to the array
            self.treasures.append(treasure)
        }
    }
    
    func moveTreasure(){
        if (self.shoreBackground.position.y + self.shoreBackground.frame.height / 2) > 0 {
            if !movingUp {
                for treasure in self.treasures {
                    treasure.position.y = treasure.position.y + self.gameMovement.y
                }
            } else {
                for treasure in self.treasures {
                    treasure.position.y = treasure.position.y - self.gameMovement.y
                }
            }
        }
    }
}

// for shooting
extension GameScene {
    func shootTheFishes(mousePosition: CGPoint){
        if self.isOnSurface {
            
            let gunPicked = SKSpriteNode(imageNamed: "\(self.gunName!)")
            gunPicked.size.width = 450
            gunPicked.size.height = 250
            gunPicked.position = CGPoint(x: frame.midX, y: frame.midY*0.6)
            addChild(gunPicked)
            
            for (index, caughtfish) in self.caughtFishesArray.enumerated() {
                if caughtfish.contains(mousePosition) {
                    self.countCoins(sprite: caughtfish)
                    
                    //SOUND EFFECT
                    self.run(bulletOnTgtSound)
                    
                    removeChildren(in: [caughtfish])
                    self.caughtFishesArray.remove(at: index)
                }
            }
            
            for (index, caughtfish) in self.fishesGoingDown.enumerated() {
                if caughtfish.contains(mousePosition) {
                    self.countCoins(sprite: caughtfish)
                    
                    //SOUND EFFECT
                    self.run(bulletOnTgtSound)
                    
                    removeChildren(in: [caughtfish])
                    self.fishesGoingDown.remove(at: index)
                }
            }
        }
    }
    
    func countCoins(sprite: SKSpriteNode){
        if(rareFishes.contains(textureName(texture: sprite.texture!))){
            GameScene.coins += 75 + self.bonus
        } else if (commonFishes.contains(textureName(texture: sprite.texture!))){
            GameScene.coins += 20 + self.bonus
        } else if (treasure.contains(textureName(texture: sprite.texture!))){
            GameScene.coins += 500 + self.bonus
        }
        self.coinLabel.text = "Coins: $\(GameScene.coins)"
    }
    
    func textureName(texture: SKTexture) -> String {
        var textDesc = String()
        if texture.description != nil {
            textDesc = (texture.description)
            let indexStart = textDesc.index(textDesc.startIndex, offsetBy: 13)
            let indexEnd = textDesc.index(textDesc.endIndex, offsetBy: -12)
            let textName = textDesc[indexStart..<indexEnd]
            return String(textName.replacingOccurrences(of: "'", with: ""))
        } else {
            return "nil"
        }
    }
}

extension GameScene {
    func gameOver(){
//        print("Game Over")
        self.score = self.metersReached * GameScene.coins
//        print("Score: \(self.score)")
        
        let gameOverTitle = SKLabelNode(text: "Game Over")
        gameOverTitle.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        gameOverTitle.fontColor = UIColor.white
        gameOverTitle.fontSize = 120
        gameOverTitle.fontName = "Avenir"
        addChild(gameOverTitle)
        
        self.scoreLabel = SKLabelNode(text: "Score: \(self.score)")
        self.scoreLabel.position = CGPoint(x:100, y:frame.size.height / 2 + 100)
        self.scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        self.scoreLabel.fontColor = UIColor.magenta
        self.scoreLabel.fontSize = 65
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.zPosition = 102
        addChild(self.scoreLabel)
        
        self.isGameOver = true
        
        let highScoreManager = HighScoreManager()
        highScoreManager.addNewScore(newScore: self.score)
    }
}
